var global = this;

function Game(width, height, stage) {

    // Size
    this.width = width;
    this.height = height;

    //Current screen 
    //0 - Nebula View
    //1 - System View
    //2 - Planet View
    this.currentScreen =0;

    this.sunSystemSelected;
    this.planetSelected = -1;

    this.paused = false;
    this.moving = false; ///< Moving something (temporary pause)
    
    //Clicks coordinates
    this.mouseX = 0;
    this.mouseY = 0;

    //Sunsystems
    this.galaxy ="";
    
    //Initialise tween container
    this.tween = [];
    this.orbit;
    this.orbitPlace=0;
    this.testPlanet=0;
    
    /// A flag to defer initialization of game state to enable calling logic to
    /// set event handlers on object creation in deserialization.
    this.initialized = false;

}

// Initialize Game. Generate Galaxy 
Game.prototype.init = function (level) {
    this.initialized = true;
    this.galaxy = new Galaxy();
    this.galaxy.init();
};

Game.prototype.dispose = function () {
	this.paused = true;
};

// Application drawing
Game.prototype.draw = function () {
    
    if(this.currentScreen == 0){ // Galaxy View
        this.galaxy.draw();
    }
    if(this.currentScreen == 1){ // SunSystem View
        this.galaxy.sunSystems[this.sunSystemSelected].draw();
    }
    if(this.currentScreen == 2){ 
        if(this.planetSelected==-1){ // Sun View
            this.galaxy.sunSystems[this.sunSystemSelected].stars[0].drawStarScreen();
        } 
        else{ // Planet View
            this.galaxy.sunSystems[this.sunSystemSelected].planets[this.planetSelected].drawPlanetScreen();
        }
    }
    stage.update();
};