/**
 * Planet
 */
function Planet(star,sunSystemNumber,planetNumber){
    Entity.call(this);
    this.star = star;
    this.tween="";
    this.orbit;
    this.orbitDistance;
    this.orbitPlace = RANDOM.next(0,79);
    this.scale;
    this.sunSystemNumber = sunSystemNumber;
    this.planetNumber = planetNumber;
    this.fullScaleBitmap;
    this.infoContainer = new createjs.Container();
    this.tmp = 0;
    var tmpCalc = this.planetNumber+1;
    this.name =GALAXY_NAME+"-"+this.sunSystemNumber+"S-"+tmpCalc+"P";

}

Planet.prototype.init = function (stage) {
	// Setting Planet Properties
	this.speedFactor = RANDOM.next(1,40);
    this.type = this.getType();
	this.getOrbit(this.type);
	this.testOrbit();
	this.orbit = this.calculateOrbit();
	this.chooseBitmap(this.type);
    this.color = this.getColor(this.type);
    this.mass = this.getMass(this.type);
    this.temperatur  = this.getTemperatur(this.type);
    this.radius = this.getRadius(this.type);
    this.characteristics = this.getCharacteristics(this.type);
    this.rotationSpeed = this.getRotationSpeed(this.type);
    this.composition = this.getComposition(this.type);
    
    // Set planeet scale
    this.scale = RANDOM.next(4,8)/100;

    // Adding back buton element
    this.backButton = createText("Back");
    this.backButton.x = 200 ;
    this.backButton.y = 900;
    this.initInfoContainer();
	this.initTween();
};

// Choose bitmap for planet (Depends on Planet Type)
Planet.prototype.chooseBitmap = function (type) {
	switch(type){
		case "Carbon":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(211,239)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Chthonian":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(211,239)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Coreless":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(179,210)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Desert":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(179,210)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Dwarf":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(179,210)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Earth analog":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(71,92)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Gas Giant":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(117,178)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Helium":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(94,116)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Hot Jupiter":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(117,178)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Ice Giant":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(1,38)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Iron":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(179,210)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Lava":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(39,70)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Mini Neptune":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(117,178)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Ocean Planet":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(1,38)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
		case "Terrestrial Planet":
			var bitmap = BITMAP_CONTAINER[3][RANDOM.next(71,92)].clone();
			this.fullScaleBitmap = bitmap.clone();
			Entity.prototype.init.call(this,bitmap.clone());
			break;
			
	}

}

Planet.prototype.initTween = function () {
	this.bitmap.scaleX = this.scale;
    this.bitmap.scaleY = this.scale;
   	this.bitmap.addEventListener('click',this.eventHandling.bind(null,this.planetNumber));
   	
	this.tween = createjs.Tween.get(this.bitmap, {loop: true});
	
	var xMinus = (this.bitmap.getBounds().width/2)*this.scale;
 	var yMinus = (this.bitmap.getBounds().height)*this.scale;
	var count = 80;
	
	for(var i=this.orbitPlace; count>0; i++) {
		if(i==79)i=0;
		
		xt = this.orbit[i].x-xMinus;
	    yt = this.orbit[i].y-yMinus;
	    if(count==80)this.tween.to({x: xt, y:yt}, 1, createjs.Ease.sineIn);	
		else this.tween.to({x: xt, y:yt}, 1000 + 100*this.speedFactor );
 	
		count--;
	};
}

Planet.prototype.handleComplete = function () {
}

Planet.prototype.removeTweens = function () {
	createjs.Tween.removeTweens(this.tween);	
}

// Set Planet Info Container (Depends on Planet Type)
Planet.prototype.initInfoContainer = function () {
	
	var frame = new createjs.Graphics();
	frame.beginFill('black').drawRect(0, 0, 420,680 );
	var frameShape = new createjs.Shape(frame);
	frameShape.x = 1210;
	frameShape.y = 150;
	this.infoContainer.addChild(frameShape);

	var frameImg = BITMAP_CONTAINER[5][2].clone();
	frameImg.x = 1180;
    frameImg.y = 130;
    this.infoContainer.addChild(frameImg);

	var obj = createText(this.name);
	obj.x = 1275;
	obj.y = 180;
	this.infoContainer.addChild(obj);

	obj = createText("Type: "+this.type,20);
	obj.x = 1250;
	obj.y = 250;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
	
	obj = createText("Mass: "+this.mass+" ME",20);
	obj.x = 1250;
	obj.y = 330;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

 	obj = createText("Radius: "+this.radius+" RE",20);
	obj.x = 1250;
	obj.y = 370;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
 	
	obj = createText("Temperatur: "+this.temperatur+" K",20);
	obj.x = 1250;
	obj.y = 410;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
 		
	obj = createText("Rotation Speed: "+this.rotationSpeed+" days",20);
	obj.x = 1250;
	obj.y = 450;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

	obj = createText("Composition: "+this.composition,20);
	obj.x = 1250;
	obj.y = 490;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
};

Planet.prototype.dispose = function (stage) {
    Entity.prototype.dispose.call(this, stage);
    createjs.Tween.removeTweens(this.bitmap);
};

// Draw Planet Orbit
Planet.prototype.drawOrbit = function(){
	var g = new createjs.Graphics();
    g.setStrokeStyle(1);
    g.beginStroke(createjs.Graphics.getRGB(192,192,192));
    g.alpha=0.1;
    
    var wid = this.planetPosition*100 + 400;
    var hei = wid/2;

    g.drawEllipse(0,0,wid,hei);
   
    var s = new createjs.Shape(g);
    s.x = CENTER_X-wid/2;
    s.y = CENTER_Y-hei/2;
    stage.addChild(s);
}

// Draw Planet Orbit Points
Planet.prototype.drawOrbitPoints = function(){
	for (var i = 0; i < 20; i++) {
        g = new createjs.Graphics();
        g.setStrokeStyle(1);
        g.beginStroke(createjs.Graphics.getRGB(0,0,0));
        g.beginFill(createjs.Graphics.getRGB(255,0,0));
        g.drawCircle(0,0,3);

        s = new createjs.Shape(g);
        s.x = this.orbit[i].x;
        s.y = this.orbit[i].y;
        stage.addChild(s);
       };
    for (var i = 20; i < 40; i++) {
        g = new createjs.Graphics();
        g.setStrokeStyle(1);
        g.beginStroke(createjs.Graphics.getRGB(0,0,0));
        g.beginFill(createjs.Graphics.getRGB(255,0,0));
        g.drawCircle(0,0,3);

        s = new createjs.Shape(g);
        s.x = this.orbit[i].x;
        s.y = this.orbit[i].y;
        stage.addChild(s);
    
    };
    for (var i = 40; i < 60; i++) {
        g = new createjs.Graphics();
        g.setStrokeStyle(1);
        g.beginStroke(createjs.Graphics.getRGB(0,0,0));
        g.beginFill(createjs.Graphics.getRGB(255,0,0));
        g.drawCircle(0,0,3);

        s = new createjs.Shape(g);
        s.x = this.orbit[i].x;
        s.y = this.orbit[i].y;
        stage.addChild(s);
    };

    for (var i = 60; i < 80; i++) {
        g = new createjs.Graphics();
        g.setStrokeStyle(1);
        g.beginStroke(createjs.Graphics.getRGB(0,0,0));
        g.beginFill(createjs.Graphics.getRGB(255,0,0));
        g.drawCircle(0,0,3);

        s = new createjs.Shape(g);
        s.x = this.orbit[i].x;
        s.y = this.orbit[i].y;
        stage.addChild(s);
    };
}

Planet.prototype.draw = function(){
  	// IF tween is inactive. Activate it
  	if(this.tween==""){
  		this.initTween();
  	}
    
    stage.addChild(this.bitmap);

}

Planet.prototype.eventHandling = function(planetNumber){
	game.currentScreen=2;
   	game.planetSelected = planetNumber;
}

Planet.prototype.drawPlanetScreen = function(){
	stage.removeAllChildren();

	// Draw Planet Backgorund
    stage.addChild(game.galaxy.sunSystems[this.sunSystemNumber].backgroundBitmap);
    
    // Draw Planet image 
    this.fullScaleBitmap.scaleX = 1.5;
    this.fullScaleBitmap.scaleY = 1.5;
    this.fullScaleBitmap.x = CENTER_X-(this.fullScaleBitmap.getBounds().width)*this.fullScaleBitmap.scaleX;
    this.fullScaleBitmap.y = CENTER_Y-this.fullScaleBitmap.getBounds().height+this.fullScaleBitmap.getBounds().height/6;
    stage.addChild(this.fullScaleBitmap);

    // Draw Info Container
    stage.addChild(this.infoContainer);

    // Add eventlister for the back button
    this.backButton.addEventListener('click',function(){
        game.currentScreen = 1;
        game.planetSelected = -1;
    });
    
    stage.addChild(this.backButton);
}

Planet.prototype.testOrbit = function(){
 	var generated = false;
 	var trys = 0;
 	while(generated==false){
	 	var od = this.orbitDistance;

	 	var place;
	 	if(od<0.1)place =1;
	 	else if(0.1<od && od<0.25)place =2;
	 	else if(0.25<od && od<0.5)place =3;
	 	else if(0.5<od && od<1.3)place =4;
	 	else if(1.3<od && od<1.7)place =5;
	 	else if(1.7<od && od<2)place =6;
	 	else if(2<od && od<2.75)place =7;
	 	else if(2.75<od && od<3.25)place =8;
	 	else if(3.25<od && od<22)place =9;
	 	else if(22<od && od<46)place =10;
	 	else if(46<od)place =11;
	 	if( game.galaxy.sunSystems[this.sunSystemNumber].planetsPositions.indexOf(place) == -1){
	 		game.galaxy.sunSystems[this.sunSystemNumber].planetsPositions.push(place);
	 		generated = true;
	 	}
	 	else{
	 		
	 		if(trys>=10){
	 			trys=0;
	 			this.type = this.getType();
	 		}
	 		this.getOrbit(this.type);
	 		generated = false;
	 		trys++;
	 	}
	}
	this.planetPosition = place;

}

// Calculates planet orbit
Planet.prototype.calculateOrbit = function(){

 	var w = this.planetPosition*100 + 400;
 	var h = w/2;
 	var x = CENTER_X-w/2, y = CENTER_Y-h/2;
    var k = 0.5522848;
    var ox = (w / 2) * k;
    var oy = (h / 2) * k;
    var xe = x + w;
    var ye = y + h;
    var xm = x + w / 2;
    var ym = y + h / 2;
    var orbitPoints = [];
    
    var bezier = new Bezier(x,ym, x,ym-oy, xm-ox,y, xm,y);
    orbitPoints.push.apply(orbitPoints,bezier.getLUT(19));

    bezier = new Bezier(xm,y, xm+ox, y, xe, ym-oy, xe, ym);
    orbitPoints.push.apply(orbitPoints,bezier.getLUT(19));
    
    bezier = new Bezier(xe, ym, xe, ym+oy, xm+ox, ye, xm, ye);
    orbitPoints.push.apply(orbitPoints,bezier.getLUT(19));
    
    bezier = new Bezier(xm, ye,xm-ox, ye, x, ym+oy, x, ym);
    orbitPoints.push.apply(orbitPoints,bezier.getLUT(19));
    
    return orbitPoints; 
}

// Get Planet Type
Planet.prototype.getType = function(){
	var type = "";
	switch(RANDOM.next(1,15)){
		case 1:
			type = "Carbon";
			break;
		case 2:
			type = "Chthonian";
			break;
		case 3:
			type = "Coreless";
			break;
		case 4:
			type = "Desert";
			break;
		case 5:
			type = "Dwarf";
			break;
		case 6:
			type = "Earth analog";
			break;
		case 7:
			type = "Gas Giant";
			break;
		case 8:
			type = "Helium";
			break;
		case 9:
			type = "Hot Jupiter";
			break;
		case 10:
			type = "Ice Giant";
			break;
		case 11:
			type = "Iron";
			break;
		case 12:
			type = "Lava";
			break;
		case 13:
			type = "Mini Neptune";
			break;
		case 14:
			type = "Ocean Planet";
			break;
		case 15:
			type = "Terrestrial Planet";
			break;
	}
	return type;
}

// Get Planet Mass (Depending on Planet Type)
// Calcualted in Earth mass - ME
Planet.prototype.getMass = function(type){
	var mass = "";
	switch(type){
		case "Carbon":
			mass = RANDOM.next(150,300)/100;
			break;
		case "Chthonian":
			mass = RANDOM.next(50,200)/100;
			break;
		case "Coreless":
			// The predicted sizes of coreless and cored planets are similar within a few percent
			mass = RANDOM.next(50,200)/100;
			break;
		case "Desert":
			// theoretical mass of terrestrial planet
			mass = RANDOM.next(50,200)/100;
			break;
		case "Dwarf":
			// 0.0022  0.00022
			mass = RANDOM.next(22,220)/100000;
			break;
		case "Earth analog":
			// Earth analog 0.7-1.2
			mass = RANDOM.next(70,120)/100;
			break;
		case "Gas Giant":
			// 500 Me (1 to 1.6 MJ mJupiter)
			mass = RANDOM.next(25000,50000)/100;
			break;
		case "Helium":
			// Helium planets are predicted to have roughly the same diameter as hydrogen–helium planets of the same mass
			mass = RANDOM.next(24,100)/100
			break;
		case "Hot Jupiter":
			mass = RANDOM.next(25000,50000)/100;
			break;
		case "Ice Giant":
			mass = RANDOM.next(25000,50000)/100;
			break;
		case "Iron":
			// Iron-rich planets are smaller and more dense than other types of planets of comparable mass
			mass = RANDOM.next(70,120)/100;
			break;
		case "Lava":
			// given their small enough mass
			mass = RANDOM.next(20,40)/100;
			break;
		case "Mini Neptune":
			// up to 10 Earth masses (M⊕), as low as one to as high as 20 M⊕
			mass = RANDOM.next(100,2000)/100;
			break;
		case "Ocean Planet":
			mass = RANDOM.next(70,1000)/100;
			break;
		case "Terrestrial Planet":
			//" errestrial Planet
			mass = RANDOM.next(70,120)/100;
			break;
			
	}
	return mass;
}

// Get Planet Color
Planet.prototype.getColor = function(type){
	var color = "";
	switch(type){
		case "Carbon":
			color = "Silver";
			break;
		case "Chthonian":
			color = "Red";
			break;
		case "Coreless":
			color = "Gray";
			break;
		case "Desert":
			color = "Yellow";
			break;
		case "Dwarf":
			color = "Brown";
			break;
		case "Earth analog":
			color = "Green";
			break;
		case "Gas Giant":
			color = "Light Brown";
			break;
		case "Helium":
			color = "white";
			break;
		case "Hot Jupiter":
			color = "Red";
			break;
		case "Ice Giant":
			color = "Blue";
			break;
		case "Iron":
			color = "Gray";
			break;
		case "Lava":
			color = "Red";
			break;
		case "Mini Neptune":
			color = "Blue";
			break;
		case "Ocean Planet":
			color = "Blue";
			break;
		case "Terrestrial Planet":
			color = "Green";
			break;
			
	}
	return color;
}

// Get planet Temperature (Depending on Planet Type)
Planet.prototype.getTemperatur = function(type){
	var temperatur = "";
	switch(type){
		case "Carbon":
			temperatur = RANDOM.next(15000,25000);
			break;
		case "Chthonian":
			temperatur = RANDOM.next(55,250);
			break;
		case "Coreless":
			temperatur = RANDOM.next(206,350);
			break;
		case "Desert":
			// theoretical temperatur of terrestrial planet
			temperatur = RANDOM.next(206,350);
			break;
		case "Dwarf":
			// PLUTO 44K, 167K Ceres
			temperatur = RANDOM.next(35,170);
			break;
		case "Earth analog":
			//307K 255K 206 K
			temperatur = RANDOM.next(206,350);
			break;
		case "Gas Giant":
			// 20,000 K
			temperatur = RANDOM.next(15000,25000);
			break;
		case "Helium":
			temperatur = RANDOM.next(15000,25000);
			break;
		case "Hot Jupiter":
			// high surface temperature
			temperatur = RANDOM.next(15000,25000);
			break;
		case "Ice Giant":
			// Ice Giant
			temperatur = RANDOM.next(55,250);
			break;
		case "Iron":
			temperatur = RANDOM.next(55,250);
			break;
		case "Lava":
			// Lava
			temperatur = RANDOM.next(1500,5000);
			break;
		case "Mini Neptune":
			// Mini Neptune
			temperatur = RANDOM.next(15000,25000);
			break;
		case "Ocean Planet":
			// boiling point, the water will become supercritical and lack a well-defined surface.[3] Even on cooler water-dominated planets, the atmosphere can be much thicker than that of Earth, and composed largely of water vapor, producing a very strong greenhouse effect.
			temperatur = RANDOM.next(274,373)
			break;
		case "Terrestrial Planet":
			// and in the habitable zone
			temperatur = RANDOM.next(206,350);
			break;
			
	}
	return temperatur;
}

// Get Planet radius (Depending on type)
//Earth radii Ri
Planet.prototype.getRadius = function(type){
	var radius = "";
	switch(type){
		case "Carbon":
			radius = RANDOM.next(80,300)/100;;
			break;
		case "Chthonian":
			radius = RANDOM.next(80,300)/100;
			break;
		case "Coreless":
			radius = RANDOM.next(80,300)/100;;
			break;
		case "Desert":
			radius = RANDOM.next(80,300)/100;;
			break;
		case "Dwarf":
			radius = RANDOM.next(10,120)/100;
			break;
		case "Earth analog":
			radius = RANDOM.next(80,164)/100;
			break;
		case "Gas Giant":
			radius = RANDOM.next(170,440)/100;
			break;
		case "Helium":
			radius = RANDOM.next(300,900)/100;
			break;
		case "Hot Jupiter":
			radius = RANDOM.next(80,300)/100;
			break;
		case "Ice Giant":
			radius = RANDOM.next(80,300)/100;
			break;
		case "Iron":
			radius = RANDOM.next(50,300)/80;
			break;
		case "Lava":
			radius = RANDOM.next(20,55)/100;
			break;
		case "Mini Neptune":
			radius = RANDOM.next(80,90)/100;
			break;
		case "Ocean Planet":
			radius = RANDOM.next(80,300)/100;
			break;
		case "Terrestrial Planet":
			radius = RANDOM.next(80,200)/100;
			break;
			
	}
	return radius;
}

// Get Planet Characteristics (Dependes on Planet Type)
Planet.prototype.getCharacteristics = function(type){
	var characteristics = "";
	switch(type){
		case "Carbon":characteristics = "A carbon planet is a theoretical type of planet that contains more carbon than oxygen.[1]. Marc Kuchner and Sara Seager coined the term 'carbon planet' in 2005 and investigated such planets following the suggestion of Katharina Lodders that Jupiter formed from a carbon-rich core.[2] Prior investigations of planets with high carbon-to-oxygen ratios C/O include Fegley & Cameron 1987.[3] Carbon planets could form if protoplanetary discs are carbon-rich and oxygen-poor. They would develop differently from Earth, Mars, and Venus, which are composed mostly of silicon–oxygen compounds. The theory is now built on reasonable scientific ideas and has gained support.[4] Different planetary systems have different carbon-to-oxygen ratios, with the Solar System's terrestrial planets are closer to being 'oxygen planets'. The unconfirmed exoplanet PSR J1719-1438 b, discovered on August 25, 2011, as well as the planet 55 Cancri e, could be such planets.";
			break;
		case "Chthonian":
			characteristics = "A chthonian planet (/ˈkθoʊniən/, sometimes 'cthonian') is a hypothetical class of celestial objects resulting from the stripping away of a gas giant's hydrogen and helium atmosphere and outer layers, which is called hydrodynamic escape. Such atmospheric stripping is a likely result of proximity to a star. The remaining rocky or metallic core would resemble a terrestrial planet in many respects.[1]";
			break;
		case "Coreless":
			characteristics = "A coreless planet is a theoretical type of terrestrial planet that has no metallic core, i.e. the planet is effectively a giant rocky mantle. Despite the name this is not the same as a hollow earth.";
			break;
		case "Desert":
			characteristics = "A desert planet or dry planet is a theoretical type of terrestrial planet with very little water. The concept has become a common setting in science fiction,[1] appearing as early as the 1956 film Forbidden Planet and Frank Herbert's 1965 novel Dune. A 2011 study suggested that not only are life-sustaining desert planets possible, but that they might be more common than Earth-like planets.[5] The study found that, when modeled, desert planets had a much larger habitable zone than watery planets";
			break;
		case "Dwarf":
			characteristics = "A dwarf planet is a planetary-mass object that is neither a planet nor a natural satellite. That is, it is in direct orbit of the Sun, and is massive enough for its gravity to crush itself into a hydrostatic equilibrium shape (usually a spheroid), but has not cleared the neighborhood of other material around its orbit";
			break;
		case "Earth analog":
			characteristics = "An Earth analog (also referred to as a Twin Earth, Earth Twin, Second Earth, Alien Earth, Earth 2 or Earth-like planet) is another planet or moon with environmental conditions similar to those found on the planet Earth.";
			break;
		case "Gas Giant":
			characteristics = "A gas giant is a giant planet composed mainly of hydrogen and helium. Jupiter and Saturn are the Solar System's gas giants. The term 'gas giant' was originally synonymous with 'giant planet', but in the 1990s it became known that Uranus and Neptune are really a distinct class of giant planet, being composed mainly of heavier volatile substances (which are referred to as 'ices'). For this reason, Uranus and Neptune are now often classified in the separate category of ice giants.";
			break;
		case "Helium":
			characteristics = "A helium planet is a planet with a helium-dominated atmosphere. This is in contrast to ordinary gas giants such as Jupiter and Saturn, whose atmospheres consist primarily of hydrogen, with helium as a secondary component only. Helium planets might form in a variety of ways. The Gliese 436 b exoplanet candidates as a helium planet";
			break;
		case "Hot Jupiter":
			characteristics = "Hot Jupiters (also called roaster planets,[1] epistellar jovians,[2][3] pegasids[4][5] or pegasean planets) are a class of extrasolar planets whose characteristics are similar to Jupiter, but that have high surface temperatures because they orbit very close[6]—between approximately 0.015 and 0.5 astronomical units (2.2×106 and 74.8×106 km)—to their parent stars,[7] whereas Jupiter orbits its parent star (the Sun) at 5.2 astronomical units (780×106 km), causing low surface temperatures.";
			break;
		case "Ice Giant":
			characteristics = "An ice giant is a giant planet composed mainly of elements heavier than hydrogen and helium, such as oxygen, carbon, nitrogen, and sulfur. There are two ice giants in the Solar System, Uranus and Neptune. They consist of only about 20% hydrogen and helium in mass, as opposed to the Solar System's gas giants (Jupiter and Saturn), which are both more than 90% hydrogen and helium in mass. In the 1990s, it was realized that Uranus and Neptune are a distinct class of giant planet, separate from the other giant planets. They have become known as ice giants because their constituent compounds were ices when they were primarily incorporated into the planets during their formation, either directly in the form of ices or trapped in water ice. The amount of solid volatiles within the ice giants today is, however, very small.[1]t";
			break;
		case "Iron":
			characteristics = "An iron planet is a type of planet that consists primarily of an iron-rich core with little or no mantle. Mercury is the largest celestial body of this type in the Solar System (as the other terrestrial planets are silicate planets), but larger iron-rich exoplanets may exist. Such a planet is also known as a Cannonball.[1]";
			break;
		case "Lava":
			characteristics = "A lava planet is a hypothetical type of terrestrial planet, with a surface mostly or entirely covered by molten lava. Situations where such planets could exist include a young terrestrial planet just after its formation, a planet that has recently suffered a large collision event, or a planet orbiting very close to its star, causing intense irradiation and tidal forces.[1]";
			break;
		case "Mini Neptune":
			characteristics = "A mini-Neptune (sometimes known as a gas dwarf or transitional planet) is a planet of up to 10 Earth masses (M⊕), smaller than Uranus and Neptune, which have about 14.5 M⊕ and 17 M⊕, respectively. Mini-Neptunes have thick hydrogen–helium atmospheres, probably with deep layers of ice, rock or liquid oceans (made of water, ammonia, a mixture of both, or heavier volatiles). These planets have small cores made of low-density volatiles.[citation needed]Without a thick atmosphere, it would be classified as an ocean planet instead.";
			break;
		case "Ocean Planet":
			characteristics = "An ocean planet, aquaplanet or water world[1] is a hypothetical type of exoplanet that has a substantial fraction of its mass made of water. The surface on such planets would be completely covered with an ocean of water hundreds of kilometers deep, much deeper than the oceans of Earth.";
			break;
		case "Terrestrial Planet":
			characteristics = "Terrestrial Planet";
			break;
			
	}
	return characteristics;
}

// Get orbit Distance
// Calculated IN AU distance from sun to Earth
Planet.prototype.getOrbit = function(type){
	var orbit = "";
	switch(type){
		case "Carbon":
			this.orbitDistance = RANDOM.next(1500,3000)/100;
			break;
		case "Chthonian":
			this.orbitDistance = RANDOM.next(1500,3000)/100;
			break;
		case "Coreless":
			// Such planets may form in cooler regions farther from the central star
			this.orbitDistance = RANDOM.next(1500,3000)/100;
			break;
		case "Desert":
			// hot desert planets without runaway greenhouse effect can exist in 0.5 AU around Sun-like stars
			this.orbitDistance = RANDOM.next(30,60)/100;
			break;
		case "Dwarf":
			// 39.17 AU.  A dwarf planet is a planetary-mass object that is neither a planet nor a natural satellite. That is, it is in direct orbit of the Sun, and is massive enough for its gravity to crush itself into a hydrostatic equilibrium shape (usually a spheroid), but has not cleared the neighborhood of other material around its orbit Orbit period Pluto: YEARS 248.09
			this.orbitDistance = RANDOM.next(1800,5300)/100;
			break;
		case "Earth analog":
			// Earth analog 
			this.orbitDistance = RANDOM.next(80,180)/100;
			break;
		case "Gas Giant":
			// jupiter orbits its parent star (the Sun) at 5.2 astronomical units (780×106 km)
			this.orbitDistance = RANDOM.next(200,1000)/100;
			break;
		case "Helium":
			this.orbitDistance = RANDOM.next(1500,3000)/100;
			break;
		case "Hot Jupiter":
			// 0.015 and 0.5 astronomical units (2.2×106 and 74.8×106 km) circular orbits
			this.orbitDistance = RANDOM.next(15,500)/1000;
			break;
		case "Ice Giant":
			//"Ice Giant";
			this.orbitDistance = RANDOM.next(1000,2000)/100;
			break;
		case "Iron":
			// Since water and iron are unstable over geological timescales, wet iron planets in the goldilocks zone may be covered by lakes of iron carbonyl and other exotic volatiles rather than water.[4]
			this.orbitDistance = RANDOM.next(15,500)/1000;;
			break;
		case "Lava":
			// a planet orbiting very close to its star
			this.orbitDistance = RANDOM.next(50,120)/100;
			break;
		case "Mini Neptune":
			// Such planets are not found orbiting too close to their parent stars
			this.orbitDistance = RANDOM.next(400,1200)/100;
			break;
		case "Ocean Planet":
			//boiling point, the water will become supercritical and lack a well-defined surface.[3] Even on cooler water-dominated planets, the atmosphere can be much thicker than that of Earth, and composed largely of water vapor, producing a very strong greenhouse effect
			this.orbitDistance = RANDOM.next(80,120)/100;
			break;
		case "Terrestrial Planet":
			// A terrestrial planet, telluric planet or rocky planet is a planet that is composed primarily of silicate rocks or metals. Within the Solar System, the terrestrial planets are the inner planets closest to the Sun, i.e. Mercury, Venus, Earth, and Mars. The terms 'terrestrial planet' and 'telluric planet' are derived from Latin words for Earth (Terra and Tellus), as these planets are, in terms of composition, 'Earth-like'
			this.orbitDistance = RANDOM.next(80,120)/100;
			break;
			
	}
	
}

// Get Planet Rotation Speed (Dependes on Planet Type)
Planet.prototype.getRotationSpeed = function(type){
	var rotationSpeed = "";
	switch(type){
		case "Carbon":
			rotationSpeed = RANDOM.next(100,5400)/100;
			break;
		case "Chthonian":
			rotationSpeed = RANDOM.next(100,5400)/100;
			break;
		case "Coreless":
			rotationSpeed = RANDOM.next(100,5400)/100;
			break;
		case "Desert":
			rotationSpeed = RANDOM.next(100,300)/100;
			break;
		case "Dwarf":
			rotationSpeed = RANDOM.next(150,370)/100;
			break;
		case "Earth analog":
			rotationSpeed = RANDOM.next(80,100)/100;
			break;
		case "Gas Giant":
			rotationSpeed = RANDOM.next(40,70)/100;
			break;
		case "Helium":
			rotationSpeed = RANDOM.next(50,120)/100;
			break;
		case "Hot Jupiter":
			rotationSpeed = RANDOM.next(20,50)/100;
			break;
		case "Ice Giant":
			rotationSpeed = RANDOM.next(300,10000)/100;
			break;
		case "Iron":
			rotationSpeed = RANDOM.next(500,5500)/100;;
			break;
		case "Lava":
			rotationSpeed = RANDOM.next(40,500)/100;;
			break;
		case "Mini Neptune":
			rotationSpeed = RANDOM.next(100,300)/100;
			break;
		case "Ocean Planet":
			rotationSpeed = RANDOM.next(100,300)/100;
			break;
		case "Terrestrial Planet":
			rotationSpeed = RANDOM.next(80,120)/100;
			break;
			
	}
	return rotationSpeed;
}

// Get Planet Composition 
Planet.prototype.getComposition = function(type){
	var composition = "";
	switch(type){
		case "Carbon":
			composition = "Iron- or steel-rich core. Surrounding that would be molten silicon carbide and titanium carbide ";
			break;
		case "Chthonian":
			composition = "Stripping away of a gas giant's hydrogen and helium atmosphere.  Rocky or metallic core ";
			break;
		case "Coreless":
			composition = "No metallic core, i.e. the planet is effectively a giant rocky mantle. ";
			break;
		case "Desert":
			composition = "A desert planet or dry planet is a theoretical composition of terrestrial planet with very little water";
			break;
		case "Dwarf":
			composition = " A dwarf planet is a planetary-mass object that is neither a planet nor a natural satellite.";
			break;
		case "Earth analog":
			composition = "Another planet or moon with environmental conditions similar to those found on the planet Earth";
			break;
		case "Gas Giant":
			composition = "A gas giant is a giant planet composed mainly of hydrogen and helium.";
			break;
		case "Helium":
			composition = "A helium planet might form via hydrogen evaporation from a gaseous planet orbiting close to a star.";
			break;
		case "Hot Jupiter":
			composition = "Class of extrasolar planets whose characteristics are similar to Jupiter";
			break;
		case "Ice Giant":
			composition = "Giant planet composed mainly of elements heavier than hydrogen and helium, such as oxygen, carbon, nitrogen, and sulfur.";
			break;
		case "Iron":
			composition = "Iron-rich core with little or no mantle. ";
			break;
		case "Lava":
			composition = " Extensive sulfur concentrated on their surfaces that is associated with the continuous active volcanism.";
			break;
		case "Mini Neptune":
			composition = "Thick hydrogen–helium atmospheres, probably with deep layers of ice, rock or liquid oceans.";
			break;
		case "Ocean Planet":
			composition = "A hypothetical type of exoplanet that has a substantial fraction of its mass made of water.";
			break;
		case "Terrestrial Planet":
			composition = "Primarily of silicate rocks or metals. solid planetary surface.  A central metallic core, mostly iron, with a surrounding silicate mantle";
			break;
			
	}
	return composition;
}
