/**
 * SunSystem
 */
function SunSystem(number){
    Entity.call(this);

    // Maxinmum planets
    this.MAX_PLANETS = 10;

    this.stars = new Array();
    this.planets = new Array();
    this.galaxyBitmap;
    this.backgroundBitmap;
    this.number = number;
    this.planetsPositions = new Array();
}

SunSystem.prototype.init = function () {
    // Set Sunsystem background image
    this.backgroundBitmap = BITMAP_CONTAINER[4][RANDOM.next(0,SYSTEM_IMAGES_COUNT-1)].clone();
    
    //Set back button
    this.backButton = createText("Back");
    this.backButton.x = 200 ;
    this.backButton.y = 900;

    // Create Star
    this.createStars();
    this.initStars();

    //Create Planets
    this.createPlanets(this);
    this.initPlanets();

    //Initialize positions
    this.initPosition();

};

SunSystem.prototype.initPosition = function (){
    function generate(sunSystem){
        sunSystem.galaxyBitmap = sunSystem.stars[0].bitmap.clone();
        sunSystem.galaxyBitmap.x = RANDOM.next(1,1920);
        sunSystem.galaxyBitmap.y = RANDOM.next(1,1080);
        sunSystem.galaxyBitmap.scaleX = 0.04;
        sunSystem.galaxyBitmap.scaleY = 0.04;
        return sunSystem;
    }
    var generated;
    var canPush = false;
    while(!canPush){
        generate(this);
        for (var i = 0; i < game.galaxy.sunSystems.length; i++) {
            if(game.galaxy.sunSystems[i].galaxyBitmap!=undefined){        
                if(game.galaxy.sunSystems[i].galaxyBitmap.x!=0 && game.galaxy.sunSystems[i].galaxyBitmap.y!=0
                    && game.galaxy.sunSystems[i].galaxyBitmap.x!=this.galaxyBitmap.x 
                    && game.galaxy.sunSystems[i].galaxyBitmap.y!=this.galaxyBitmap.y){
                    if(ndgmr.checkRectCollision(this.galaxyBitmap,game.galaxy.sunSystems[i].galaxyBitmap)==null) canPush=true;
                    else{canPush=false;} 
                }
                else canPush = true;
            }
        }  
    }
}

SunSystem.prototype.createStars = function (params){
    var stars = 1;
/*  if(RANDOM.nextProz(5)){ // Having 5% chnace of multiple stars system
        stars = 2;
    }*/
    for (var i = 1; i <= stars; i++) {
        this.stars.push(new Star(this.number));
    };
};

SunSystem.prototype.initStars = function (params){
    for(var i = 0; i<this.stars.length; i++){
        this.stars[i].init();
    };
};

SunSystem.prototype.createPlanets = function (params){
    var planets = RANDOM.next(0,this.MAX_PLANETS);
    
    for (var i = 0; i < planets; i++) {
        this.planets.push(new Planet(this.stars[0],this.number,i));
    }
};

SunSystem.prototype.initPlanets = function (params){
  
    for(var i = 0; i<this.planets.length; i++){
        this.planets[i].init();
    };
};

SunSystem.prototype.dispose = function (stage) {
    Entity.prototype.dispose.call(this, stage);
    createjs.Tween.removeTweens(this.bitmap);
};

SunSystem.prototype.draw = function () {
    
    stage.removeAllChildren();

    stage.addChild(this.backgroundBitmap);

/*    //DRAW GRID
    var xi =100;
    var yi = 50;
    for (i = 0; i < 11; i++) {
      for (j = 0; j < 11; j++) {
        //tILE draw
        var bmp = BITMAP_CONTAINER[5][0].clone();
    
        bmp.x = ((j-i) * xi+stageDimWidth/2);
        bmp.y = ((i+j) * yi);//+stageDimHeight/8);
        bmp.regX = xi;
        bmp.regY = yi;
    
        stage.addChild(bmp);
    }}
*/    

/*    // DRAW ORBITS
    for (var i = 0; i < this.planets.length; i++) {
            this.planets[i].drawOrbit();
    }*/

/*    // DRAW ORBITAL POINTS
    for (var i = 0; i < this.planets.length; i++) {
            this.planets[i].drawOrbitPoints();
    }
    */

    // DRAW STAR
    this.stars[0].draw();

    // DRAW PLANETS
    for (var i = 0; i < this.planets.length; i++) {
            this.planets[i].draw();
    }

    // Add back button listener
    this.backButton.addEventListener('click',function(){

        game.currentScreen = 0;

        for(var i = 0;i<game.galaxy.sunSystems[game.sunSystemSelected].planets.length;i++){
           game.galaxy.sunSystems[game.sunSystemSelected].planets[i].removeTweens();
        }

    });
    stage.addChild(this.backButton);
    
}