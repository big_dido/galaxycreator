/// Custom inheritance function that prevents the super class's constructor
/// from being called on inehritance.
/// Also assigns constructor property of the subclass properly.
function inherit(subclass, base) {
    // If the browser or ECMAScript supports Object.create, use it
    // (but don't remember to redirect constructor pointer to subclass)
    if (Object.create) {
        subclass.prototype = Object.create(base.prototype);
    }
    else {
        var sub = function () {
        };
        sub.prototype = base.prototype;
        subclass.prototype = new sub;
    }
    subclass.prototype.constructor = subclass;
}


function isoTo2D(point){
  var twoDPoint= {x:0,y:0};
  twoDPoint.x = (2 * point.y + point.x) / 2;
  twoDPoint.y = (2 * point.y - point.x) / 2;
  return(twoDPoint);
}

function twoDToIso(point,pit){
  if(pit==1)
    console.log(point.x);
  var isoPoint = {x:0,y:0};
  isoPoint.x = point.x - point.y;
  isoPoint.y = (point.x + point.y) / 2;
  return(isoPoint);
}