/**
 * SunSystem
 */
function Galaxy(){
    Entity.call(this);
    
    // Sun Systems array
    this.sunSystems = new Array();

    // Galaxy infotext
    this.infoText = new createjs.Container();

    // Sun Sustem count
    this.SUNSYTEMS_MIN_COUNT = 15;
    this.SUNSYTEMS_MAX_COUNT = 32;

}

// Initialize Galaxy object
Galaxy.prototype.init = function(){
    
    // Set galaxy background
    Entity.prototype.init.call(this,BITMAP_CONTAINER[1][RANDOM.next(0,GALAXY_IMAGES_COUNT-1)].clone());

    // Create Sun Systems objects
    this.createSunSystems();   

    // Initialize Sun systems
    this.initSunSystems();

    this.fillInfoText();
}

// Information table for the Galaxy
Galaxy.prototype.fillInfoText = function(){
    var creationText = createText("Sun Systems: "+this.sunSystems.length);
    creationText.x = 0 ;
    creationText.y = 0 ;
    this.infoText.addChild(creationText);

    for (var i = 0; i < this.sunSystems.length; i++) {
        
        creationText = createText("Sun: "+this.sunSystems[i].stars.length);
        creationText.x = 0 ;
        creationText.y =  (i+1)*30;
        this.infoText.addChild(creationText);
        creationText = createText("Planets: "+this.sunSystems[i].planets.length);
        creationText.x = 150 ;
        creationText.y = (i+1)*30 ;

        this.infoText.addChild(creationText);    
    };
}

Galaxy.prototype.createSunSystems = function(){

    var systems = RANDOM.next(this.SUNSYTEMS_MIN_COUNT,this.SUNSYTEMS_MAX_COUNT);

    for (var i = 0; i < systems; i++) {
        this.sunSystems.push(new SunSystem(i));    
    };
}

Galaxy.prototype.initSunSystems = function(){

    for (var i = 0; i < this.sunSystems.length; i++) {
        this.sunSystems[i].init();
    };
};

Galaxy.prototype.draw = function(){
    
    stage.removeAllChildren();
    stage.addChild(this.bitmap);
    stage.addChild(this.infoText);

    // Drawing SunSystems. Represented by the Sun
    for (var i = 0; i < this.sunSystems.length; i++) {
        
        // Adding listener for img click  
        this.sunSystems[i].galaxyBitmap.addEventListener('click', this.eventHandling.bind(null,i));
                
        stage.addChild(this.sunSystems[i].galaxyBitmap);
        stage.update();
    }

  
};

// Handle SunSystem click
Galaxy.prototype.eventHandling = function(i){
    game.currentScreen = 1;
    game.sunSystemSelected = i;
}

Galaxy.prototype.dispose = function (stage) {
    createjs.Tween.removeTweens(this.bitmap);
};